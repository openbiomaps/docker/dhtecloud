<?php
$CONFIG = array (
  'trusted_domains' =>
  array (
    0 => 'subdomain.your.server.com',
  ),
  'datadirectory' => '/var/www/nextcloud',
  'mail_domain' => 'some.mailserver.com',
  'mail_from_address' => 'obm-admin',
  'mail_smtpmode' => 'smtp',
  'mail_smtphost' => 'smtp.some.mailserver.com',
  'mail_smtpauthtype' => 'LOGIN',
  'overwritehost' => 'subdomain.your.server.com',
  'overwrite.cli.url' => 'https://subdomain.your.server.com',
  'overwriteprotocol' => 'https',
  'overwritecondaddr' => '',
  'overwritewebroot' => '',
  'loglevel' => 0,
  'connectivity_check_domains' =>
  array (
    0 => 'www.nextcloud.com',
    1 => 'www.startpage.com',
    2 => 'www.eff.org',
    3 => 'www.edri.org',
    4 => 'apps.nextcloud.com',
    5 => 'updates.nextcloud.com',
    6 => 'lookup.nextcloud.com',
    7 => 'push-notifications.nextcloud.com',
  ),
  'trusted_proxies' =>
  array (
    0 => '172.24.0.2', # your traefik ip in the docker network
  ),
  'forwarded_for_headers' =>
  array (
  ),
  'mail_smtpsecure' => 'ssl',
  'mail_sendmailmode' => 'smtp',
  'mail_smtpauth' => 1,
  'mail_smtpport' => '1025', # your mailserver's smtp port
  'mail_smtpname' => 'yourusername', #your email username
  'mail_smtppassword' => 'youremailpassword',
  'memcache.distributed' => '\\OC\\Memcache\\Redis',
  'memcache.locking' => '\\OC\\Memcache\\Redis',
  'redis' =>
  array (
    'host' => 'redis',
    'port' => 6379,
    'timeout' => '0.0',
  ),
  'default_language' => 'hu', # your preferred language
  'default_locale' => 'hu_HU', # your preferred locale
  'passwordsalt' => '',
  'secret' => '',
  'installed' => false,
  'maintenance' => false,
  'ldapIgnoreNamingRules' => false,
  'ldapProviderFactory' => 'OCA\\User_LDAP\\LDAPProviderFactory',
  'app.mail.transport' => 'php-mail',
  'app.mail.imap.timeout' => 40,
  'app.mail.smtp.timeout' => 30,
  'app.mail.verify-tls-peer' => false,
  'mail_smtpdebug' => true,
  'has_rebuilt_cache' => true,
  'theme' => '',
  'instanceid' => '',
  'dbtype' => 'pgsql',
  'version' => '20.0.1.1',
  'dbname' => 'nextcloud',
  'dbhost' => 'db:5432',
  'dbport' => '',
  'dbtableprefix' => 'oc_',
  'dbuser' => 'oc_nextcloudadmin',
  'dbpassword' => '',
  'activity_expire_days' => 30,
);
