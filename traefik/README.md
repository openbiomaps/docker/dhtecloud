This is a system global proxy for docker with Let's Encrypt and http to https
redirect. Can be tested with whoami service. 

Openbiomaps docker-composer project services uses labels to dynamically
configure traefik inners (router, midleware, etc.). 

Adust domains and authentication datas in appropriate places of config files.

Usage:

- clone this repository on OpenBioMaps server

`https://gitlab.com/openbiomaps/docker/traefik2.0-proxy.git`

- in rules/api.yml adjust domain name and htpasswd:

`htpasswd -bn traefik changeMe`

- in traefik.yml use your email in acme certificatesResolvers

`      email: yourname@yourdomain`

- if you test with whoami, in docker-compose.yml change domain and basic.auth
values

- start proxy:

`docker-compose up -d`

- for whoami test visit http://yourdomain/whoami URL. It should redirect to
https and load whoami container informations

- if don't want to track acme.json
`git update-index --skip-worktree acme.json`
